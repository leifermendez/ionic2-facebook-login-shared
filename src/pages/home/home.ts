import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  userData:any;
  constructor(public navCtrl: NavController,private fb:Facebook) {

  }

  loginFb(){
    this.fb.login(['public_profile', 'user_friends', 'email']) //son permisos de lectura
      .then((res: FacebookLoginResponse) => {
        this.fb.login(['publish_actions']).then((re:any)=>{ //permiso escritura
          this.fb.api('me?fields=id,name,email,first_name,picture.width(720).height(720).as(picture_large)', []).then(profile => {
            this.userData = {email: profile['email'], first_name: profile['first_name'], picture: profile['picture_large']['data']['url'], username: profile['name']}
          });
        });
      })
      .catch(e => console.log('Error logging into Facebook', e));
  }

  sharedFb(){
    this.fb.showDialog({
      method: 'share',
      href: 'http://devportfolio.online',
      caption: 'Mi portafolio',
      description: 'Much description',
      picture: 'http://devportfolio.online/images/photo.jpg',
      share_sheet: true,
    }).then((res:any) => console.log('Logged into Facebook!'+res))
      .catch(e => console.log('Error logging into Facebook', e));
  }
}
